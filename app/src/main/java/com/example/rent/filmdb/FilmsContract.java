package com.example.rent.filmdb;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-07-04.
 */
// wygląd tabeli i kolumn
public final class FilmsContract {
    private FilmsContract() {
    }

    public static class Films implements BaseColumns {
        public static final String TABLE_NAME = "films";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_BUDGET = "budget";
        public static final String COLUMN_RLS = "release";

    }
}
