package com.example.rent.filmdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-04.
 */

public class DataBaseQuery {
    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;

    public DataBaseQuery(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void openWrite() {
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void openRead() {
        sqLiteDatabase = dbHelper.getReadableDatabase();
    }

    public void closeDB() {
        sqLiteDatabase.close();
    }

    public String addToDB(String title, String budget, String release) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FilmsContract.Films.COLUMN_NAME_TITLE, title);
        contentValues.put(FilmsContract.Films.COLUMN_BUDGET, budget);
        contentValues.put(FilmsContract.Films.COLUMN_RLS, release);

        long rowNumber = sqLiteDatabase.insert(FilmsContract.Films.TABLE_NAME, null, contentValues);
        return String.valueOf(rowNumber);
    }

    public List<FilmModel> filmModelList() {
        ArrayList<FilmModel> filmList =new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(FilmsContract.Films.TABLE_NAME, null, null, null, null, null, null);
        FilmModel filmModel;
        // if sprawdza czy cursor nie jest NULLem
        if(cursor.getCount() > 0){
            // przechodzimy po każdym elemencie cursora
            for(int i = 0; i < cursor.getCount(); i++){
                // przechodzi do następnego
                cursor.moveToNext();
                filmModel = new FilmModel();
                filmModel.setId(cursor.getInt(0));
                filmModel.setTitle(cursor.getString(1));
                filmModel.setBudget(cursor.getString(2));
                filmModel.setRelease(cursor.getString(3));
                filmList.add(filmModel);
            }
        }
        return filmList;
    }

    public void deleteFromDB(String id){
        String selection = FilmsContract.Films._ID + " = ?";
        String[] selectionArgument = { id };
        sqLiteDatabase.delete(FilmsContract.Films.TABLE_NAME, selection, selectionArgument);
    }

    public void editDB(String id, String budget){
        ContentValues contentValues = new ContentValues();
        contentValues.put(FilmsContract.Films.COLUMN_BUDGET, budget);

        String selection = FilmsContract.Films._ID + " = ? ";
        String[] selectionArgument = { id };
        sqLiteDatabase.update(FilmsContract.Films.TABLE_NAME, contentValues, selection, selectionArgument);
    }

    public void cleanTable(){
        sqLiteDatabase.execSQL("DELETE FROM " + FilmsContract.Films.TABLE_NAME);
    }
}
