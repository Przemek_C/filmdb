package com.example.rent.filmdb;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    DataBaseQuery dataBaseQuery;
    @BindView(R.id.textView)
    TextView textView;

    @OnClick(R.id.addBtn)
    public void addtoDataBase(){
        String transformers = dataBaseQuery.addToDB("Transformers", "200 mln $", "2015-01-01");
        textView.setText(transformers);
    }

    @OnClick(R.id.viewBtn)
    public void viewFilms(){
        viewMyFilms();
    }

    @OnClick(R.id.deleteBtn)
    public void del(){
        dataBaseQuery.deleteFromDB("1");
        viewMyFilms();
    }

    @OnClick(R.id.editBtn)
    public void edit(){
        dataBaseQuery.editDB("3", "100 mkn $");
        viewMyFilms();
    }

    @OnClick(R.id.cleanBtn)
    public void clean(){
        dataBaseQuery.cleanTable();
        viewMyFilms();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);
        ButterKnife.bind(this);
        dataBaseQuery = new DataBaseQuery(MainActivity.this);
        dataBaseQuery.openWrite();


    }
    @Override
    public void onDestroy(){
        dataBaseQuery.closeDB();
        super.onDestroy();
    }

    private void viewMyFilms() {
        List<FilmModel> filmModels = dataBaseQuery.filmModelList();
        String text = "";
        for (FilmModel filmModel : filmModels){
            text += filmModel;
        }
        textView.setText(text.toString());
    }
}
