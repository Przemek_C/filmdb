package com.example.rent.filmdb;

/**
 * Created by RENT on 2017-07-05.
 */

public class FilmModel {
    private int id;
    private String title;
    private String budget;
    private String release;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    @Override
    public String toString(){
        String newLine = "\n";
        return "Id = " + id +
                ", Title: " + title +
                ", Budget: " + budget +
                ", Release date: " + release + newLine;
    }
}
