package com.example.rent.filmdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RENT on 2017-07-04.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final int VERSION = 3;
    public static final String DATABASE_NAME = "DataBaseFilms.bd"; //baza localna na tel.

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + FilmsContract.Films.TABLE_NAME
            + " (" + FilmsContract.Films._ID + " INTEGER PRIMARY KEY, "
            + FilmsContract.Films.COLUMN_NAME_TITLE + " TEXT, "
            + FilmsContract.Films.COLUMN_BUDGET + " TEXT, "
            + FilmsContract.Films.COLUMN_RLS + " TEXT)";

    public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + FilmsContract.Films.TABLE_NAME + ";";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }
}
